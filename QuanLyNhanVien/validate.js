function showMessage(spanID,message){
    document.getElementById(spanID).innerText=message;

}
function kiemTraDoDai(spanID,message,account){
const regex=/^\d{4,6}$/;
var re=regex.test(account)
    if(re){
        showMessage(spanID,"");
        return true;
    
    }else{
        showMessage(spanID,message);
        return false
    }
}
function kiemTraText(spanID,message,hoTen){
    const regex = /^[^\d\s!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?`~]+(\s[^\d\s!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?`~]+)*$/;;
    var isName=regex.test(hoTen);
    if(isName){
        showMessage(spanID,"");
        return true
    }else{
        showMessage(spanID,message);
    }
}
function kiemTraEmail(spanID,message,email){
    const regex= /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail=regex.test(email);
    if(isEmail){
     showMessage(spanID, "");
     return true
    }else{
     showMessage( spanID ,message);
     return false;
    }

}
function kiemTraMatKhau(spanID,message,matKhau){
    const regex=/^(?=.*\d)(?=.*[A-Z])(?=.*[\W_]).{6,10}$/;
    var isValid=regex.test(matKhau);
    if(isValid){
        showMessage(spanID,"");
        return true
}else{
    showMessage(spanID,message);
    return false
}
}
function kiemTraNgayLam(spanID,message,ngayLam){
    const regex=/^(0[1-9]|1[0-2])\/(0[1-9]|[12][0-9]|3[01])\/(?:(?!0000)\d{4})$/;
    var isValid=regex.test(ngayLam);
    if(isValid){
        showMessage(spanID,"");
        return true
}else{
    showMessage(spanID,message);
    return false
}
}
function kiemTraLuongCoBan(spanID,message,luong){
    if(luong>=1000000 && luong<=20000000){
        showMessage(spanID,"");
        return true
}else{
    showMessage(spanID,message);
    return false
}
}
function kiemTraChucVu(spanID,message,chucVi){
   
    if(chucVi=="Sếp" || chucVi=="Trưởng phòng" || chucVi =="Nhân viên"){
        showMessage(spanID,"");
        return true
}else{
    showMessage(spanID,message);
    return false
}
}
function kiemTraSoGioLam(spanID,message,soGioLam){
    if(soGioLam>=80 && soGioLam<=200){
        showMessage(spanID,"");
        return true
}else{
    showMessage(spanID,message);
    return false
}}